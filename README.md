# DPU PoC - IPsec BF to Server

### Description

This is an Ansible Playbook for a DPU PoC that creates an IPsec tunnel between a Bluefield-2(+) DPU and an Ubuntu 20.04 server

### IMPORTANT

The DPU IPsec demo requires that the Bluefield-2(+) is setup in Embedded mode. Go to the following demo:

https://gitlab-master.nvidia.com/mcourtney/dpu-poc-kit#running-the-playbooks

and run the following playbook:

```
ansible-playbook poc-embedded-mode.yml
```

### Notes

This demo was built using the instructions found here:

https://docs.mellanox.com/display/BlueFieldSWv36011699/IPsec+Functionality

### Caveats

1. This demo has only been tested in Embedded mode.
2. Improvements need to be made to offload tunneled traffic in this demo

### Instructions

1. First, re-install the BFB on the Bluefield-2(+) and set the card into Embedded mode

2. Edit the "hosts" file and set the appropriate "ipsecsrc" and "ipsecdest" for the BF and the Ubuntu server

3. Run the playbook with the following command:

```
ansible-playbook dpu-poc-bf-to-server.yml
```

The output will look like the following:

```
output
```

4. On the Bluefield-2(+), run the following command:

```
sudo swanctl -i --child bf
```

This above will configure and install IPsec on the Bluefield(2)+ and the Ubuntu 20.04 with StrongSwan

### Troubleshooting StrongSwan

Useful commands on the Ubuntu host:

```
sudo swanctl --stats
sudo swanctl -l
sudo swanctl --list-sas
```

Useful commands on the BF2:

```
sudo ipsec status
sudo swanctl -l
sudo swanctl --log
sudo ipsec statusall
sudo swanctl -t --ike BFL-BFR
```

### Troubleshooting the DPU

1. The DPUs are currently limited in terms of troubleshooting. If you're having an issue, it is best to start from scratch and re-run the Install / PoC Kit:

https://confluence.nvidia.com/display/NetworkingBU/SA+DPU+PoC+Kit

There is a "reinstall" playbook which reflashes the BFB without the need to re-run the entire PoC Kit:

```
ansible-playbook reinstall-bfb.yml
```
